FROM centos

USER 0

RUN yum install -y wget  gcc tcl-devel make && \
    useradd eggdrop

USER eggdrop 
    
RUN cd && \
    wget https://ftp.eggheads.org/pub/eggdrop/source/1.8/eggdrop-1.8.4.tar.gz && \
    tar xf eggdrop-1.8.4.tar.gz && \
    cd eggdrop-1.8.4 && \
    ./configure && \
    sed -i -e 's:^MAKE=:MAKE=/usr/bin/make:g' Makefile && \
    make config && \
    make && \
    make install 

USER 0

RUN yum remove -y wget gcc make

ADD eggdrop.conf /home/eggdrop/eggdrop/eggdrop.conf
ADD AmstaBot.user /home/eggdrop/eggdrop/AmstaBot.user

RUN chown -R :root /home/eggdrop && \
    chmod -R g=u /home/eggdrop

ENTRYPOINT [ "/home/eggdrop/eggdrop/eggdrop" ]
CMD [ "-n", "-m", "/home/eggdrop/eggdrop/eggdrop.conf" ]

    
     

